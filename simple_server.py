import requests
import socket
import select
import sys

import pdb

class SimpleServer:

    # dev
    dev = True
    verbose = True

    def __init__(self, addr, inStream):
        self.addr = ('', 8080)
        self.inStream = inStream #sys.stdin

        self._sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._sock.bind(addr)


    def start(self):
        self._sock.listen(5)
        clients = []
        running = 1
        if self.verbose:
            print "starting server on %s:%s" % self.addr

        selInput = [self._sock, self.inStream] #used by select() to wait for new clients and keyboard input

        while running:
            inReady, outReady, error = select.select(selInput, [], [])

            for s in inReady:
                if s == self._sock:
                    '''
                    sock input event means a new client is trying to contact the server. 
                    The server calls accept() to get the new client and then appends the client to its list of input sockets. 
                    This means that next time it calls select() it can handle any input the client has sent on this new socket.
                    '''

                    newClient, address = self._sock.accept()

                    if self.verbose:
                        print "new connection from ", address
                    selInput.append(newClient)
                    clients.append(newClient)

                elif s == self.inStream:
                    '''
                    instream keyboard event
                    '''
                    # pdb.set_trace()
                    data = s.readline()
                    if self.verbose:
                        print "keyboard event ", data
                    for client in clients:
                        client.send(data)
                else:
                    '''
                    handle any client requests
                    '''

                    data = s.recv(1024)
                    if data:
                        if self.verbose:
                            print data
                            s.send("HTTP/1.1 200 OK\r\n\r\n")
                            r = requests.get("http://google.com")
                            s.send(r.content)
                            s.send('\r\n\r\n')
                    else:
                        s.close()
                        selInput.remove(s)
                        if self.verbose:
                            print "conn clossed"

if __name__ == "__main__":
    server = SimpleServer(('',8080), sys.stdin)

    server.start()

