import socket
import select
import sys
import time

import pdb
import ptth
import http

class Agent:

    dev = True
    verbose = True

    def __init__(self, proxyControlAddr, proxyDataAddr):
        self.proxyDataAddr = proxyDataAddr
        self.proxyConrolAddr = proxyControlAddr
        try:
            self._controlSock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self._controlSock.connect(proxyControlAddr)
            print 'connected to %s:%s', proxyControlAddr
        except socket.error as e:
            import pdb; pdb.set_trace()
            print "could not connect to server"

    def start(self):
        running = 1
        print "running"
        selInputs = [self._controlSock]
        while running:
            try:
                inready, outready, error = select.select(selInputs, [], [])

                for origin in inready:

                    if origin == self._controlSock:
                        data = origin.recv(1024*10)
                        if data:
                            print data
                            '''
                            incoming request from controll
                            decode to http req
                            make request on it's behalf
                            '''
                            httpstr = data
                            ptth.requestFromString(httpstr)
                            r = http.requestFromString(httpstr)
                            response = r.send()
                            if response:
                                '''
                                encode  resp to ptth
                                '''
                                dataSock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                                dataSock.connect(self.proxyDataAddr)
                                dataSock.send(response)
                                dataSock.recv(1024)
                                time.sleep(1)
                                dataSock.close()
                        else:
                            print 'control conn closed'
                            try:
                                pass
                                time.sleep(5)
                                self._controlSock.connect(self.proxyConrolAddr)
                            except socket.error:
                                print "conrol disconnected and attempt to reconnect failed"
                                break

            except KeyboardInterrupt:
                print 'keyboard interupt'
                self._controlSock.close()
                break

if __name__ == "__main__":
    agent = Agent(('localhost', 8082), ('localhost', 8083))
    agent.start()
